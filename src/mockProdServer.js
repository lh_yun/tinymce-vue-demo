import { createProdMockServer } from '../es/createProdMockServer'
import ephoxSpelling from '../mock/ephox-spelling'

export function setupProdMockServer() {
  createProdMockServer([...ephoxSpelling])
}
