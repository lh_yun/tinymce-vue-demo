/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2022 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("zh_CN", {
    "Autocorrect": "\u81ea\u52a8\u6821\u6b63",
    "Auto correct {0}": "\u81ea\u52a8\u6821\u6b63{0}",
    "Capitalization": "\u5927\u5199"
});
