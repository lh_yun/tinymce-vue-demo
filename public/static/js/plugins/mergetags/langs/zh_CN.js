/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2022 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("zh_CN", {
    "Merge tag": "Merge tag",
    "Insert merge tag": "\u63d2\u5165 merge tag",
    "Filter merge tags": "\u7b5b\u9009 merge tags",
    "No matches": "\u65e0\u5339\u914d\u9879"
});
