/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2022 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("zh_CN", {
    "Save code": "\u4fdd\u5b58\u4ee3\u7801",
    "Cancel": "\u53d6\u6d88"
});