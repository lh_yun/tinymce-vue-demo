/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2022 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("zh_CN", {
    "Rotate counterclockwise": "\u9006\u65f6\u9488\u65cb\u8f6c",
    "Rotate clockwise": "\u987a\u65f6\u9488\u65cb\u8f6c",
    "Flip vertically": "\u5782\u76f4\u7ffb\u8f6c",
    "Flip horizontally": "\u6c34\u5e73\u7ffb\u8f6c",
    "Edit image": "\u7f16\u8f91\u56fe\u7247",
    "Image options": "\u56fe\u7247\u9009\u9879",
    "Zoom in": "\u653e\u5927",
    "Zoom out": "\u7f29\u5c0f",
    "Crop": "\u526a\u88c1",
    "Resize": "\u8c03\u6574\u5927\u5c0f",
    "Orientation": "\u65b9\u5411",
    "Brightness": "\u4eae\u5ea6",
    "Sharpen": "\u9510\u5316",
    "Contrast": "\u5bf9\u6bd4\u5ea6",
    "Color levels": "\u989c\u8272\u5c42\u6b21",
    "Gamma": "\u4f3d\u9a6c\u503c",
    "Invert": "\u53cd\u8f6c",
    "Apply": "\u5e94\u7528",
    "Back": "\u8fd4\u56de",
    "R": "R",
    "G": "G",
    "B": "B",
    "Width": "\u5bbd\u5ea6",
    "Height": "\u9ad8\u5ea6",
    "Save": "\u4fdd\u5b58",
    "Cancel": "\u53d6\u6d88"
});
