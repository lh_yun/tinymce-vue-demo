/**
 * @file
 *  ephox-hyperlinking.js
 * @Component
 *  Enhanced Media Embed(https://www.tiny.cloud/docs/tinymce/6/introduction-to-mediaembed/)
 *  Link Checker(https://www.tiny.cloud/docs/tinymce/6/linkchecker/)
 * @Description
 *  Link Checker and Enhanced Media Embed service for TinyMCE Enterprise.
 */

let linkChecking = {
    allowedOrigins: {// 允许与服务器端编辑器功能通信的域,通配符支持，正则表达式支持
        origins: ["myserver:8000", "*.mydomain.example.com", "/(myserver|myotherserver\.)?example\.com/"],
        sameOrigin: false, // 启用此选项后，如果服务是从同一服务器部署的，则无需指定访问服务的域源,该选项可用于阻止所有跨源请求,默认false
        ignorePort: false// 忽略端口，默认true
    },
    enabled: true,//   启用 (true) or 禁用 (false），默认true
    fallbackToGet: true,// 识别非标准HEAD, 默认false（method: 'get'）
    cache: {
        capacity: 500, // 设置缓存的容量。缺省设置为 500
        timeToLiveInSeconds: 86400,// 设置缓存元素的生存时间，以秒为单位。这是允许元素保留在缓存中的最长时间。默认设置为86400秒，即一天
        timeToIdleInSeconds: 3600,// 设置缓存元素的空闲时间，以秒为单位。这是元素在未被访问时保留在缓存中的最长时间。默认设置为3600秒，即一小时。
    }
};

let embed = {
    iframely: {
        enabled: true,//   启用 (true) or 禁用 (false）,如果false则不需要配置baseUrl、apiKey
        baseUrl: "https://iframe.ly/api/iframely",// iframe API的基准URL。如果你不确定，可以查看他们（https://iframely.com/docs/iframely-api）的文件。
        apiKey: "xxx" // 你的Iframely API密钥。这是Iframely在与他们建立帐户后提供的。
    },
    custom: [
        {
            endpoint: "http://localhost:3000/oembed",// 插入具有与 中的条目匹配的 URL 的 URL 的媒体时应查阅的 oEmbed 端点的 URL schemes
            schemes: [
                "http://intranet.example.com/*" // 第2.1节定义的方案列表(https://oembed.com/#section2.1)。oEmbed规范的配置。注意，HTTP和HTTPS是两个独立的方案。
            ]
        }
    ]
}

export default [
    /**
     * 链接检查器linkchecker https://www.tiny.cloud/docs/tinymce/6/linkchecker/
     * "linkchecker_service_url": "https://hyperlinking.tiny.cloud",
     */
    {
        url: '/hyperlinking/1/check',
        method: 'get',
        response: ({query}) => {
            let req = query || {url: '', fresh: false};
            let opt = {
                model: 'blacklist', // 默认白名单: blacklist
                blacklist: ['localhost'], //白名单
                whitelist: ['127.0.0.1', '192.168.0.1'], //黑名单
                valid: (url) => {
                    let valid = opt[opt.model].some(host => url.includes(host))
                    return opt.model === 'blacklist' ? valid : !valid;
                }
            }
            let result = (url) => {
                return opt.valid(url) ? "VALID" : "INVALID";
            }
            let res = {"url": req.url, "result": result(req.url)};
            return res;
        }
    },
    {
        url: '/hyperlinking/1/check',
        method: 'post',
        response: ({body}) => {
            let req = body || {urls: [{url: '', fresh: false}]};
            let opt = {
                model: 'blacklist', // 默认白名单: blacklist
                blacklist: ['localhost'], //白名单
                whitelist: ['127.0.0.1', '192.168.0.1'], //黑名单
                valid: (url) => {
                    let valid = opt[opt.model].some(host => url.includes(host))
                    return opt.model === 'blacklist' ? valid : !valid;
                }
            }
            let result = (url) => {
                return opt.valid(url) ? "VALID" : "INVALID";
            }
            let results = (urls) => {
                return urls.map(({url}) => {
                    return {"url": url, "result": result(url)};
                })
            }
            let check = (req) => {
                return {"results": [...results(req.urls)]}
            }
            let res = check(req);
            return res;
        }
    },
    /**
     * 媒体嵌入mediaembed https://www.tiny.cloud/docs/tinymce/6/introduction-to-mediaembed/
     * "mediaembed_service_url": "/hyperlinking",
     */
    {
        url: '/hyperlinking/1/embed',
        method: 'post',
        response: ({body}) => {
            let req = body || {url: '', maxWidth: 0};
            return {
                "title": "The Most Advanced WYSIWYG Editor | Trusted Rich Text Editor | TinyMCE",
                "canonicalIri": "https://www.tiny.cloud/",
                "shortIri": null,
                "description": "TinyMCE is the most advanced WYSIWYG HTML editor designed to simplify website content creation. The rich text editing platform that helped launched Atlassian, Medium, Evernote and more.",
                "date": null,
                "authorName": null,
                "authorIri": null,
                "providerName": "TinyMCE",
                "providerIri": null,
                "rels": {
                    "primary": "summary",
                    "technical": [],
                    "source": [
                        "iframely",
                        "script_censor"
                    ],
                    "unknown": []
                },
                "links": {
                    "players": [],
                    "thumbnails": [
                        {
                            "media": {
                                "type": "fixed",
                                "width": 1200,
                                "height": 630,
                                "padding_bottom": null
                            },
                            "rels": {
                                "primary": "thumbnail",
                                "technical": [
                                    "ssl"
                                ],
                                "source": [
                                    "iframely"
                                ],
                                "unknown": [
                                    "og"
                                ]
                            },
                            "href": "https://www.tiny.cloud/images/tiny-v5-og-image.png",
                            "mime_type": "image/png",
                            "html": "<img src=\"https://www.tiny.cloud/images/tiny-v5-og-image.png\" />"
                        }
                    ],
                    "apps": [],
                    "images": [],
                    "readers": [],
                    "surveys": [],
                    "summary_cards": [
                        {
                            "media": null,
                            "rels": {
                                "primary": "summary",
                                "technical": [],
                                "source": [
                                    "iframely",
                                    "script_censor"
                                ],
                                "unknown": []
                            },
                            "href": "https://www.tiny.cloud/",
                            "mime_type": "text/html",
                            "html": "<div class=\"ephox-summary-card\">\n  <a class=\"ephox-summary-card-link-thumbnail\" href=\"https://www.tiny.cloud/\">\n    <img class=\"ephox-summary-card-thumbnail\" src=\"https://www.tiny.cloud/images/tiny-v5-og-image.png\" />\n  </a>\n  <a class=\"ephox-summary-card-link\" href=\"https://www.tiny.cloud/\">\n    <span class=\"ephox-summary-card-title\">The Most Advanced WYSIWYG Editor | Trusted Rich Text Editor | TinyMCE</span>\n    <span class=\"ephox-summary-card-description\">TinyMCE is the most advanced WYSIWYG HTML editor designed to simplify website content creation. The rich text editing platform that helped launched Atlassian, Medium, Evernote and more.</span>\n    \n    <span class=\"ephox-summary-card-website\">TinyMCE</span>\n  </a>\n</div>"
                        }
                    ],
                    "icons": [
                        {
                            "media": {
                                "type": "fixed",
                                "width": 32,
                                "height": 32,
                                "padding_bottom": null
                            },
                            "rels": {
                                "primary": "icon",
                                "technical": [
                                    "ssl"
                                ],
                                "source": [
                                    "iframely"
                                ],
                                "unknown": []
                            },
                            "href": "https://www.tiny.cloud/favicon-32x32.png",
                            "mime_type": "image/png",
                            "html": "<img src=\"https://www.tiny.cloud/favicon-32x32.png\" />"
                        },
                        {
                            "media": {
                                "type": "fixed",
                                "width": 16,
                                "height": 16,
                                "padding_bottom": null
                            },
                            "rels": {
                                "primary": "icon",
                                "technical": [
                                    "ssl"
                                ],
                                "source": [
                                    "iframely"
                                ],
                                "unknown": []
                            },
                            "href": "https://www.tiny.cloud/favicon-16x16.png",
                            "mime_type": "image/png",
                            "html": "<img src=\"https://www.tiny.cloud/favicon-16x16.png\" />"
                        }
                    ],
                    "logos": [
                        {
                            "media": {
                                "type": "fixed",
                                "width": 1200,
                                "height": 630,
                                "padding_bottom": null
                            },
                            "rels": {
                                "primary": "thumbnail",
                                "technical": [
                                    "ssl"
                                ],
                                "source": [
                                    "iframely"
                                ],
                                "unknown": [
                                    "og"
                                ]
                            },
                            "href": "https://www.tiny.cloud/images/tiny-v5-og-image.png",
                            "mime_type": "image/png",
                            "html": null
                        }
                    ],
                    "promos": [],
                    "files": []
                },
                "media": null,
                "html": "<div class=\"ephox-summary-card\">\n  <a class=\"ephox-summary-card-link-thumbnail\" href=\"https://www.tiny.cloud/\">\n    <img class=\"ephox-summary-card-thumbnail\" src=\"https://www.tiny.cloud/images/tiny-v5-og-image.png\" />\n  </a>\n  <a class=\"ephox-summary-card-link\" href=\"https://www.tiny.cloud/\">\n    <span class=\"ephox-summary-card-title\">The Most Advanced WYSIWYG Editor | Trusted Rich Text Editor | TinyMCE</span>\n    <span class=\"ephox-summary-card-description\">TinyMCE is the most advanced WYSIWYG HTML editor designed to simplify website content creation. The rich text editing platform that helped launched Atlassian, Medium, Evernote and more.</span>\n    \n    <span class=\"ephox-summary-card-website\">TinyMCE</span>\n  </a>\n</div>"
            }
        }
    }
]
