import * as https from "https";

/**
 * @file
 *  ephox-image-proxy.js
 * @Component
 *  Export(https://www.tiny.cloud/docs/tinymce/6/export/)
 *  Enhanced Image Editing(https://www.tiny.cloud/docs/tinymce/6/editimage/)
 * @Description
 *  Image proxy service for the Export and Enhanced Image Editing plugins.
 */
let imageProxy = {
    imageSize: 10000000 // 10MB in bytes
}

/**
 * 读网络文件并转发
 * @param res
 * @param url
 */
const networkFileProxy = (url) => {
    return new Promise((resolve, reject) => {
        const req = https.get(url, (res) => {
            res.setEncoding('binary');
            let data = '';
            res.on('data', buffer => data += buffer);
            res.on('end', () => resolve(data));
        })
        req.on('error', (e) => reject(e));
        req.end();
    })
}

export default [
    /**
     * 图像编辑editimage https://www.tiny.cloud/docs/tinymce/6/editimage/
     * "editimage_proxy_service_url": "https://imageproxy.tiny.cloud"
     * 导出export https://www.tiny.cloud/docs/tinymce/6/export/
     * "export_image_proxy_service_url": "https://imageproxy.tiny.cloud",
     */
    {
        url: '/imageproxy/2/image',
        method: 'get',
        response: ({query}) => {
            let req = query || {url: ''};
            return networkFileProxy(req.url);
        }
    },
]
