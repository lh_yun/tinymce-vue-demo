export default [
    /**
     * tiny云盘 tinydrive https://www.tiny.cloud/docs/tinymce/6/tinydrive-introduction/
     *  "tinydrive_service_url": "https://catalog.tiny.cloud"
     */
    {
        url: '/catalog',
        method: 'get',
        response: ({query}) => {
            return query;
        }
    }
]
