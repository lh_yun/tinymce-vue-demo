export default [
    /**
     * 实时协作Real-Time Collaboration https://www.tiny.cloud/docs/tinymce/6/rtc-introduction/
     * "rtc_service_url": "https://rtc.tiny.cloud",
     */
    {
        url: '/rtc',
        method: 'get',
        response: ({query}) => {
            return query;
        }
    }
]
